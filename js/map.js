import { LitElement, html, css, customElement, property, PropertyValues, unsafeCSS } from "lit-element";
import * as L from "leaflet/dist/leaflet-src.esm";
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import leafletCss from 'leaflet/dist/leaflet.css';
import ResizeObserver from 'resize-observer-polyfill';

/* Workaround for fixing icons with webpack */
const DefaultIcon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow
});

L.Marker.prototype.options.icon = DefaultIcon;

export class Map extends LitElement {

  static get properties() {
      return {
          latitude: {type: String},
          longitude: {type: String},
          zoom: {type: Number}
      }
  }
  static get styles() {
    return [
      css`
        // :host {
        //   display: block;
        //   overflow: hidden;
        //   height: 100%;
        //   flex: 1;
        // }
        #map {
          height: 300px;
          width: 300px
        }
      `,
      css`
        ${unsafeCSS(leafletCss)}
      `
    ];
  }

  render() {
    return html`
      <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
      <div id="map"></div>
    `;
  }
  constructor() {
    super();
    this.latitude = 0;
    this.longitude = 0;
    this.zoom = 9;
  }

  firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);
    const apiTokenFromMapbox =
      "pk.eyJ1IjoiYXJ0dXItIiwiYSI6ImNqczR3ZmZjdTA2bG0zeXFrZzUyZWZzOG4ifQ.imLHDDke9wglhq-afKaLAg";
    this.initMap(apiTokenFromMapbox);
  }

  initMap(apiToken) {
    this.map = L.map(this.shadowRoot.querySelector("#map"));
    this.map.setView([this.latitude, this.longitude], this.zoom);
    L.tileLayer(
      "https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}",
      {
        attribution:
          'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: "mapbox.streets",
        accessToken: apiToken
      }
    ).addTo(this.map);
    L.marker([this.latitude, this.longitude]).addTo(this.map)

    const observer = new ResizeObserver(() => {
      this.map.invalidateSize();
    });
    observer.observe(this.map.getContainer());

  }

}
customElements.define("atk-map", Map)