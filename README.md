# Map Web Component

This web component is developed using [Leaflet.js](https://leafletjs.com/) and [Lit Element](https://lit-element.polymer-project.org/guide).
```<atk-map>```  takes latitude, longitde and zoom level as input and puts a marker at the that position.
```<atk-one-map>``` takes a list of objects ({"latitude":0, "longitude":0}) as input and puts markers at each positiom.